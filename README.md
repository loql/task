# The task
- - -

## Get started:

`npm install`

`bower install`

`grunt serve`

- - -

## Main changes:

* app/scripts/app.js  -- The angular code

* app/views/main.html -- Page template

* app/index.html      -- Main html 

- - -