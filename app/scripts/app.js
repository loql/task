'use strict';

var app = angular.module('taskApp', ['ngRoute', 'ngSanitize']);




app.config(['$routeProvider',
  function($routeProvider) {

    $routeProvider

    .when('/', {
      templateUrl: 'views/main.html',
      controller: 'MainCtrl'
    })

    .otherwise({
      redirectTo: '/'
    })
  }
])




app.factory('DataService', ['$http', '$q',
  function($http, $q) {

    return {

      getEntries: function() {

        var defer = $q.defer();

        /**
         * Altinative:
         *
         * var url = "http://admin.workbench.strakertranslations.com/dummy/data.json?callback=JSON_CALLBACK";
         * $http.jsonp(url).success(function(data){defer.resolve(data)}
         */
        $http.get('data.json').success(function(data) {

          defer.resolve(data);

        }).error(function(data, status, headers, config) {

        })


        return defer.promise;
      }
    }
  }
])





app.controller('MainCtrl', ['$scope', '$sce', 'DataService',
  function($scope, $sce, DataService) {

    // Fetch entries after being involked
    DataService.getEntries().then(function(data) {

      $scope.entries = data;

      var defaultEntry = $scope.entries[0];

      $scope.activeEntry = defaultEntry;

    })


    // Assign actived entry
    $scope.showEntry = function(entry) {

      $scope.activeEntry = entry;

    }
    

    // Check if the entry is actived
    $scope.isActive = function(entry) {

      var isActive = ($scope.activeEntry === entry) ? true : false;

      return isActive;
    }


    // Escape entry body html tags
    $scope.sanitize = function() {

      if ($scope.activeEntry) {

        return $sce.trustAsHtml($scope.activeEntry.body);
      }
    }

  }
])




app.directive('postIntro', [
  function() {

    return {
      scope: {
        entry: '='
      },
      restrict: 'AE',
      replace: true,
      template: '<div class="intro">{{entry.title}}</div>',
      link: function(scope, iElement, iAttrs) {

        iElement.bind('mouseover', function() {
          iElement.css('cursor', 'pointer');
        });
      }
    };

  }
])